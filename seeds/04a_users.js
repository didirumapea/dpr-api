const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('users').del()
        .then(function () {
            // Inserts seed entries
            return knex('users').insert([
                {
                    id: 1,
                    id_organization: 1,
                    email: '1@gmail.com',
                    password: '$2b$10$9MIGKTq9dnw8TlosXbdV5.HxAnKiGKWEzD9dd/mxN.nPMa3H2IEzG',
                    name: 'Rafel Amstrong',
                    img_url: 'personal-2.png',
                    phone: '+62321321231321',
                    city_id: 1101,
                    province_id: 11,
                    birthdate: '2017-06-15',
                    gender: 'laki-laki',
                    register_type: 'manual',
                },
                {
                    id: 2,
                    id_organization: 2,
                    email: '2@gmail.com',
                    password: '$2b$10$9MIGKTq9dnw8TlosXbdV5.HxAnKiGKWEzD9dd/mxN.nPMa3H2IEzG',
                    name: 'Michael Jordan',
                    img_url: 'personal-2.png',
                    phone: '+62321321231321',
                    city_id: 1101,
                    province_id: 11,
                    birthdate: '2017-06-15',
                    gender: 'laki-laki',
                    register_type: 'manual',
                },
                {
                    id: 3,
                    id_organization: 3,
                    email: '3@gmail.com',
                    password: '$2b$10$9MIGKTq9dnw8TlosXbdV5.HxAnKiGKWEzD9dd/mxN.nPMa3H2IEzG',
                    name: 'Benitez',
                    img_url: 'personal-2.png',
                    phone: '+62321321231321',
                    city_id: 1101,
                    province_id: 11,
                    birthdate: '2017-06-15',
                    gender: 'laki-laki',
                    register_type: 'manual',
                },
                {
                    id: 4,
                    id_organization: 4,
                    email: '4@gmail.com',
                    password: '$2b$10$9MIGKTq9dnw8TlosXbdV5.HxAnKiGKWEzD9dd/mxN.nPMa3H2IEzG',
                    name: 'John Doe',
                    img_url: 'personal-2.png',
                    phone: '+62321321231321',
                    city_id: 1101,
                    province_id: 11,
                    birthdate: '2017-06-15',
                    gender: 'laki-laki',
                    register_type: 'manual',
                },
                {
                    id: 5,
                    id_organization: 2,
                    email: '5@gmail.com',
                    password: '$2b$10$9MIGKTq9dnw8TlosXbdV5.HxAnKiGKWEzD9dd/mxN.nPMa3H2IEzG',
                    name: 'Barack Olfian',
                    img_url: 'personal-2.png',
                    phone: '+62321321231321',
                    city_id: 1101,
                    province_id: 11,
                    birthdate: '2017-06-15',
                    gender: 'laki-laki',
                    register_type: 'manual',
                },
                {
                    id: 6,
                    id_organization: 5,
                    email: 'admin-stc@gmail.com',
                    password: '$2b$10$9MIGKTq9dnw8TlosXbdV5.HxAnKiGKWEzD9dd/mxN.nPMa3H2IEzG',
                    name: 'Ferguso',
                    img_url: 'personal-2.png',
                    phone: '+62321321231321',
                    city_id: 1101,
                    province_id: 11,
                    birthdate: '2017-06-15',
                    gender: 'laki-laki',
                    register_type: 'manual',
                    is_super_admin: "1"
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
