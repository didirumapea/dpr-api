const resizeImg = require('resize-img');
const fs = require('fs')

const getMaxIdSort = () => ChatContent.findOne().select('id_sort').sort({ id_sort: -1 });
const resizeBaseImg = async (file) => {
    const image = await resizeImg(fs.readFileSync(file), {
        width: 744,
        height: 432
    });
    fs.writeFileSync(file, image);
};


module.exports = {
    getMaxIdSort,
    resizeBaseImg
};