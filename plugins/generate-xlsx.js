const basepath =  __dirname.replace('plugins', '');
const moment = require('moment');
const cfg = require('../config');
const fs = require('fs');
const mkdirp = require('mkdirp');
const readXlsxFile = require('read-excel-file/node');
// require('core-js/modules/es.promise');
// require('core-js/modules/es.object.assign');
// require('core-js/modules/es.object.keys');
// require('regenerator-runtime/runtime');
// const ExcelJS = require('exceljs/dist/es5');
// polyfills required by exceljs
const ExcelJS = require('exceljs'); // problem server shuould kill with `killall node`
// console.log(basepath+'public/templates')
// #region GENERATE PROCESS REKONSILE MERCHANT VOUCHER CODE

// GEN
exports.generateExcelResponden = async (data, status, res) => {
    // var Excel = require('exceljs');
    // console.log(data)
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `responden-${data[0].client_slug}-${status === '1' ? 'complete' : 'not-complete'}-${dateNow}.xlsx`
    let filepath = cfg.assetPath + 'prs-safety-consulting-files/assets/files/report/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR With Nin';
    workbook.lastModifiedBy = 'Anony';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };

    worksheet.columns = [
        {header: 'No', key: 'no', width: 8},
        {header: 'Category Responden', key: 'category_responden', width: 32},
        {header: 'Number ID', key: 'number_id', width: 32},
        {header: 'Name', key: 'name', width: 32},
        {header: 'Email', key: 'email', width: 32},
        {header: 'Department', key: 'department', width: 32},
        {header: 'Location', key: 'location', width: 32},
        {header: 'Position', key: 'position', width: 32},
        // {header: 'Phone', key: 'phone', width: 10, outlineLevel: 1},
        // {header: 'Age', key: 'age', width: 15},
        // {header: 'Caption', key: 'caption', width: 15},
    ];

    data.forEach((element, i) => {
        worksheet.addRow(
            {
                no: i + 1,
                category_responden: element.category_responden,
                number_id: element.number_id,
                name: element.name,
                email: element.email,
                department: element.department,
                location: element.location,
                position: element.position,
            });
        // console.log(element.merchant)
        // console.log(i)
    });

    await workbook.xlsx.writeFile(filepath + filename)
        .then(function () {
            // done
            // console.log('done generate list pre register user')
            // SendEmail(filename)
            res.json({
                success: true,
                msg: 'Generate list responden success!',
                count: data.length,
                // data: data,
                path: filename
            })
        })
        .catch(err => {
            console.log(err)
            res.json({
                success: false,
                // msg: 'Generate list pre register user success!',
                // count: data.length,
                // // data: data,
                // path: filename
            })
        });
}

// SEND WITH GOOGLE ACCOUNT
exports.genExcelCetak = async (data, status, res) => {
    // var Excel = require('exceljs');
    // console.log(data)
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `SCNasabah_MEGA${data.cycle}_${data.produk}.xlsx`
    let filepath = cfg.assetPath + 'billnet-files/assets/files/output/LOG_NASABAH_CETAK/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR With Nin';
    workbook.lastModifiedBy = 'Anony';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };

    worksheet.columns = [
        {header: 'No', key: 'no', width: 8},
        {header: 'Category Responden', key: 'category_responden', width: 32},
        {header: 'Number ID', key: 'number_id', width: 32},
        {header: 'Name', key: 'name', width: 32},
        {header: 'Email', key: 'email', width: 32},
        {header: 'Department', key: 'department', width: 32},
        {header: 'Location', key: 'location', width: 32},
        {header: 'Position', key: 'position', width: 32},
        // {header: 'Phone', key: 'phone', width: 10, outlineLevel: 1},
        // {header: 'Age', key: 'age', width: 15},
        // {header: 'Caption', key: 'caption', width: 15},
    ];

    data.forEach((element, i) => {
        worksheet.addRow(
            {
                no: i + 1,
                category_responden: element.category_responden,
                number_id: element.number_id,
                name: element.name,
                email: element.email,
                department: element.department,
                location: element.location,
                position: element.position,
            });
        // console.log(element.merchant)
        // console.log(i)
    });

    await workbook.xlsx.writeFile(filepath + filename)
        .then(function () {
            // done
            // console.log('done generate list pre register user')
            // SendEmail(filename)
            // res.json({
            //     success: true,
            //     msg: 'Generate list responden success!',
            //     count: data.length,
            //     // data: data,
            //     path: filename
            // })
        })
        // .catch(err => {
        //     console.log(err)
        //     res.json({
        //         success: false,
        //         // msg: 'Generate list pre register user success!',
        //         // count: data.length,
        //         // // data: data,
        //         // path: filename
        //     })
        // });
}

exports.readExcelFile = async (path) => {
    const bulk = []
    // console.log(path)
    const workbook = new ExcelJS.Workbook();
    return await workbook.xlsx.readFile(path)
        .then(function() {
            const worksheet = workbook.getWorksheet(1); // start sheet from 1 to ...
            // console.log(worksheet.rowCount)
            worksheet.eachRow({ includeEmpty: true }, function(row, rowNumber) {
                // console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                // console.log(row.values[1]);
                bulk.push(row.values[1]) // columns 1
            });
            // console.log(bulk)
            return bulk
        });
}

