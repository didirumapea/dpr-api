const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config')
const setupPaginator = require('knex-paginator');
const checkAuthAdmin = require('../../../../middleware/check-auth-admin')
const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')
const date = require('../../../../plugins/moment-date-format')

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';
// const someOtherPlaintextPassword = 'not_bacon';
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In Auth ADMIN Route')
})

router.post('/admin/login',  (req, res) => {
    db.select(
        'id',
        'name',
        'email',
        'password',
        'role',
        'phone',
        // 'social_token',
    )
        .from('tb_admin')
        .where('email', req.body.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // console.log(data)
            if (data.length > 0){
                bcrypt.compare(req.body.password, data[0].password).then((result) => {
                    if (result){
                        const user = {
                            id: data[0].admin_id,
                            name: data[0].name,
                            email: data[0].email,
                            role: data[0].role,
                            phone: data[0].phone,
                            // social_token: data[0].social_token,
                        }
                        jwt.sign(user, config.jwtSecretKeyAdmin, {expiresIn: '9999 years'}, (err, token) => {
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            // user.token = token
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: {
                                    user,
                                    token: token
                                }
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });
            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: 'Username or password incorrect',
                data: err
            })
        })
});

router.get('/admin/logout',  (req, res) => {
    res.json({
        success: true,
        message: "User logget out",
    })
});

router.post('/admin/register', (req, res) => {
    db.select(
        'id',
        'email',
    )
        .from('tb_admin')
        .where('email', req.body.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // check REF Code IF Exist
            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                        // Store hash in your password DB.
                        req.body.password = hash
                        db('tb_admin')
                            .insert(req.body)
                            .then(data2 => {
                                // console.log(data2)
                                // console.log(data[0])
                                // Mock User
                                db.select(
                                    'id',
                                    'email',
                                    'name',
                                    'images',
                                    'phone',
                                    // 'social_token',
                                )
                                    .from('tb_admin')
                                    .where('id', data2[0])
                                    .then((data3) => {
                                        //console.log(data3)
                                        const user = {
                                            id: data2[0],
                                            email: data3[0].email,
                                            name: data3[0].name,
                                            images: data3[0].images,
                                            phone: data3[0].phone ,
                                            // social_token: data3[0].social_token,
                                            created_at: data3[0].created_at,
                                        }
                                        jwt.sign(user, config.jwtSecretKeyAdmin, {expiresIn: '9999 years'}, (err, token) => {
                                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                            user.token = token
                                            res.json({
                                                success: true,
                                                message: "Daftar admin berhasil dan login sukses",
                                                data: user
                                            });
                                        });
                                    })
                            });
                        // })
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

router.get('/admin/profile', checkAuthAdmin, (req, res) => {
    res.json({
        success: true,
        message: "User Available",
        data: req.user,
    })
});

module.exports = router;