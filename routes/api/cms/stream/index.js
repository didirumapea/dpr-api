const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment');
const fs = require('fs')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const slugify = require('slugify')
const path = require('path')
const multer  = require('multer')
const randomstring = require("randomstring");
const cryptoRandomString = require('crypto-random-string');
const changeCase = require('change-case')
const storage = multer.diskStorage(
    {
        destination: function(req, file, cb) {
            const path = `/mnt/cdn/dpr-files/assets/files/stream/${file.fieldname}`
            if (!fs.existsSync(path)){
                fs.mkdirSync(path, { recursive: true });
            }
            cb(null, path);
        },
        filename: function (req, file, cb) {
            let files = ''
            if (file) {
                if(req.body.image_name){
                    files = req.body.image_name.replace(/\.[^/.]+$/, "")
                }else{
                    files = randomstring.generate({
                        length: 20,
                        // capitalization: 'uppercase',
                    });
                }
            } else {
                files = req.body.image_name ? req.body.image_name.replace(/\.[^/.]+$/, "") : null
            }
            // console.log(file.mimetype.split('/')[1])
            // console.log(path.extname(file.originalname))
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            // cb( null, Date.now()+`-${symbol}${path.extname(file.originalname)}`);
            cb(null, files + `${path.extname(file.originalname)}`);
        }
    }
);
// '/mnt/cdn/dpr-files/assets/files/movies'
let upload = multer({ storage: storage })


router.get('/', (req, res) => {
    res.send('We Are In STREAM Route')
})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
        't2.name as category_name',
    )
        .from('tb_stream as t1')
        .innerJoin('tb_stream as t2', 't1.category_id', 't2.id')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Stream still empty' : "Success get data stream",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET BY URL
router.get('/url=:url/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_stream')
        .where({ 'url': req.params.url })
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            paginator.data.length !== 0 ? paginator.data[0].type = changeCase.titleCase(paginator.data[0].type) : null
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Stream still empty' : "Success get data stream",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data.length !== 0 ? paginator.data[0] : [],
            });
        });
});

// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .whereRaw(`CONCAT_WS('', ref_no, regarding, letter_type, agenda_no, attachment, receive_from, hit_on_last_number, hit_on_the_next_number, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get document' : 'There are no document yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});

// ADD
const cpUpload = upload.fields([{ name: 'image', maxCount: 1 }, { name: 'banner', maxCount: 1 }])
router.post('/add', cpUpload, (req, res) => {
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    if (req.files) {
        // console.log('Uploading file... '+req.file.filename);
        // let filename = req.file.filename;
        // let uploadStatus = 'File Uploaded Successfully';
        req.body.image = req.files.image ? req.files.image[0].filename : null
        req.body.banner = req.files.banner ? req.files.banner[0].filename : null
        req.body.url = cryptoRandomString({length: 10, type: 'url-safe'});
        // req.body.expired_at = moment().utc().add('1', 'year').format('YYYY-MM-DD HH:mm:ss')
        // console.log(moment().utc().add('1', 'year').format('YYYY-MM-DD HH:mm:ss'))
        db.select(
            'name',
        )
            .from('tb_stream')
            .where({
                name: req.body.name,
            })
            .then((data) => {
                if (data.length === 0){
                    // console.log(detail)
                    db('tb_stream')
                        .insert(req.body)
                        .then(data => {
                            res.json({
                                success: true,
                                message: 'Add stream success',
                                count: data,
                                // data: result,
                            });
                        })
                    return true
                }else{
                    res.json({
                        success: false,
                        message: 'Stream already exist',
                    });

                }
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Add stream failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "Upload image failed",
        });
    }
});

// UPDATE
router.patch('/update', upload.single('image'), (req, res) =>  {
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    req.file ? req.body.image = req.file.filename : req.body.image = req.body.image_name
    delete req.body.image_name
    db('tb_stream')
        .where('url', req.body.url)
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update stream video success.",
                // count: data.length,
                data: data,
            });
        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update banners failed.",
            // count: data.length,
            data: err,
        });
    });
});

// UPDATE IS PUBLIC/PRIVATE
router.patch('/update-is-public', (req, res) =>  {
    // console.log(req.body)
    db('tb_stream')
        .where('id', req.body.id)
        .update('status', req.body.is_public_private)
        .then(data => {
            res.json({
                success: true,
                message: "Update stream status success.",
                count: data.length,
                data: data,
            });
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update stream status failed",
                // count: data.length,
                data: err,
            });
        });
});

// UPDATE IS ACTIVE
router.post('/update-is-active', (req, res) =>  {
    // console.log(req.body)
    db('banners')
        .where('id', req.body.id)
        .update('is_deleted', req.body.is_deleted)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banner status failed",
                // count: data.length,
                data: err,
            });
        });

});

// DELETE
router.delete('/delete/:id', (req, res) => {
    db('tb_stream').where({ id: req.params.id })
        .del()
        .then((data) => {
        res.json({
            success: true,
            message: "Delete stream success",
            data: data,
        });
    });
});

module.exports = router;