const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
// const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const CryptoJS = require("crypto-js");
const bcrypt = require('bcrypt');
const saltRounds = 10;

router.get('/', (req, res) => {
    res.send('We Are In ADMIN Route')
})

//region ADMIN
// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
    )
        .from('tb_admin as t1')
        .orderBy('t1.'+req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: paginator.data.length === 0 ? 'Admin still empty' :'Success get data admin',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch(err => {
            console.log(err)
            res.json({
                success: false,
                message: err,
            });
        })
});

// GET BY ID
router.get('/list/id=:id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*'
    )
        .from('tb_admin as t1')
        .where({
            't1.id': req.params.id,
        })
        .orderBy('t1.'+req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: paginator.data.length === 0 ? "Admin tidak ditemukan." : 'Success get data admin by id',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data.length !== 0 ? paginator.data[0] : [],
            });
        });
});

// GET LIST COLUMN FILTER
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort/column-filter=:column_filter', (req, res) => {
    // console.log(column_filter)
    let resultDec = {
        't1.is_deleted': '0'
    }
    if (req.params.column_filter !== 'null'){
        // decrypt
        let reb64 = CryptoJS.enc.Hex.parse(req.params.column_filter);
        let bytes = reb64.toString(CryptoJS.enc.Base64);
        let decrypt = CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
        resultDec = JSON.parse(decrypt.toString(CryptoJS.enc.Utf8))
        // var plain = decrypt.toString(CryptoJS.enc.Utf8);
    }
    const entries = Object.entries(resultDec)
    const q = db.select(
        't1.*',
        't2.name as organization_name'
    )
        .from('users as t1')
        .innerJoin('master_organization as t2', 't1.id_organization', 't2.id');
    entries.forEach((element) => {
        // console.log(element[0], element[1])
        if (element[0] === 'email' || element[0] === 'name'){
            q.where('t1.'+element[0], 'like', `%${element[1]}%`)
        }else{
            if (element[0] === 'organization_name'){
                if (element[1] !== ''){
                    q.where('t2.name', element[1])
                }
            }else if (element[1] !== ''){
                q.where(element[0], element[1])
            }
        }
    })
    q.orderBy(`${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data users",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sort,
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
    )
        .from('admin as t1')
        .whereRaw(`CONCAT_WS('', t2.name, t1.email, t1.name, t1.phone) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data admin",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sort,
                data: paginator.data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get list admin failed.",
                data: err,
            });
            console.log('admin : '+err)
        });
});
// ADD
router.post('/add', (req, res) => {
    console.log(req.body)
    db.select(
        'id',
        'email',
    )
        .from('tb_admin')
        .where('email', req.body.email)
        .then(data => {
            // check REF Code IF Exist
            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                        // Store hash in your password DB.
                        req.body.password = hash
                        db('tb_admin')
                            .insert(req.body)
                            .then(data2 => {
                                res.json({
                                    success: true,
                                    message: "New admin added",
                                    // data: data,
                                })
                            });
                        // })
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

// UPDATE IS VERIFIED ADMIN
router.patch('/update', (req, res) =>  {
    db('tb_admin')
        .where({
            'id': req.body.id
        })
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update admin success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update admin failed",
                // count: data.length,
                data: err,
            });
        });

});
// UPDATE IS SUPER ADMIN
router.post('/update-is-super-admin', (req, res) =>  {
    db('users')
        .where({
            'id': req.body.id,
            id_organization: 5
        })
        .update('is_super_admin', req.body.is_super_admin)
        .then(data => {
            res.json({
                success: true,
                message: "Update super admin status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update super admin status failed",
                // count: data.length,
                data: err,
            });
        });

});

// DELETE
router.delete('/delete/:id', (req, res) => {
    db('tb_admin').where({ id: req.params.id })
        .del()
        .then((data) => {
            res.json({
                success: true,
                message: "Delete admin success",
                data: data,
            });
        });
});

//endregion

module.exports = router;