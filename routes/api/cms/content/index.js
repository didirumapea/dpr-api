const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment');
const fs = require('fs')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const slugify = require('slugify')
const path = require('path')
const multer  = require('multer')
const randomstring = require("randomstring");
const cryptoRandomString = require('crypto-random-string');
const { getVideoDurationInSeconds } = require('get-video-duration')
const { getAudioDurationInSeconds } = require('get-audio-duration');
const getMP3Duration = require('get-mp3-duration')
const resizeImg = require('resize-img');
const { resizeBaseImg } = require('../../../../plugins/helper');
const changeCase = require('change-case')
const storage = multer.diskStorage(
    {
        destination: function(req, file, cb) {
            // console.log(file.mimetype.split('/')[0])
            const path = `/mnt/cdn/dpr-files/assets/files/content/${ req.body.type }/${ file.mimetype.split('/')[0] }`
            if (!fs.existsSync(path)){
                fs.mkdirSync(path, { recursive: true });
            }
            cb(null, path);
        },
        filename: async function (req, file, cb) {
            let files = ''
            if (file) {
                if(req.body.gif){
                    files = req.body.gif.replace(/\.[^/.]+$/, "")
                }
                if(req.body.image_name){
                    files = req.body.image_name.replace(/\.[^/.]+$/, "")
                }else if (req.body.content_name){
                    files = req.body.content_name.replace(/\.[^/.]+$/, "")
                }
                else{
                    files = randomstring.generate({
                        length: 20,
                        // capitalization: 'uppercase',
                    });
                }
            } else {
                files = req.body.image_name ? req.body.image_name.replace(/\.[^/.]+$/, "") : null
                files = req.body.content_name ? req.body.content_name.replace(/\.[^/.]+$/, "") : null
            }
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            // cb( null, Date.now()+`-${symbol}${path.extname(file.originalname)}`);
            cb(null, files + `${path.extname(file.originalname)}`);
        }
    }
);
// '/mnt/cdn/dpr-files/assets/files/movies'
let upload = multer({ storage: storage })


router.get('/', (req, res) => {
    res.send('We Are In CONTENT Route')
})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't2.name as category_name',
        't1.*',

    )
        .from('tb_movies as t1')
        .innerJoin('tb_category as t2', 't1.category_id', 't2.id')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET BY URL
router.get('/url=:url/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_movies')
        .where({ 'url': req.params.url })
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // paginator.data.length !== 0 ? paginator.data[0].type = changeCase.titleCase(paginator.data[0].type) : null
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Content still empty' : "Success get data content",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data.length !== 0 ? paginator.data[0] : [],
            });
        });
});

// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .whereRaw(`CONCAT_WS('', ref_no, regarding, letter_type, agenda_no, attachment, receive_from, hit_on_last_number, hit_on_the_next_number, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get document' : 'There are no document yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});

// ADD
const cpUpload = upload.fields([{ name: 'image', maxCount: 1 }, { name: 'video', maxCount: 1 }, { name: 'gif', maxCount: 1 }])
router.post('/add', cpUpload, async (req, res) => {
    // set any image to standar pixel size
    await resizeBaseImg(req.files.image[0].path)
    // set up slug
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    // setup duration on the video
    const stream = fs.createReadStream(req.files.video[0].path)
    if (req.files.video[0].mimetype.split('/')[0] === 'video') {
        req.body.duration = await getVideoDurationInSeconds(stream)
    } else {
        const buffer = fs.readFileSync(req.files.video[0].path)
        const duration = getMP3Duration(buffer)
        req.body.duration = (duration/1000).toFixed()
    }
    if (req.files) {
        // console.log('Uploading file... '+req.file.filename);
        // let filename = req.file.filename;
        // let uploadStatus = 'File Uploaded Successfully';
        req.body.image = req.files.image ? req.files.image[0].filename : null
        req.body.video = req.files.video ? req.files.video[0].filename : null
        req.body.gif = req.files.gif ? req.files.gif[0].filename : null
        req.body.url = cryptoRandomString({length: 10, type: 'url-safe'});
        // req.body.expired_at = moment().utc().add('1', 'year').format('YYYY-MM-DD HH:mm:ss')
        // console.log(moment().utc().add('1', 'year').format('YYYY-MM-DD HH:mm:ss'))
        db.select(
            'name',
        )
            .from('tb_movies')
            .where({
                name: req.body.name,
            })
            .then((data) => {
                if (data.length === 0){
                    // console.log(detail)
                    db('tb_movies')
                        .insert(req.body)
                        .then(data => {
                            res.json({
                                success: true,
                                message: 'Add movie success',
                                count: data,
                                // data: result,
                            });
                        })
                    return true
                }else{
                    res.json({
                        success: false,
                        message: 'Movie already exist',
                        // count: data,
                        // data: result,
                    });

                }
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Add movie failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "Upload image failed",
        });
    }
});

// UPDATE

router.patch('/update', cpUpload, (req, res) =>  {
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    req.files.image ? req.body.image = req.files.image[0].filename : req.body.image = req.body.image_name
    req.files.gif ? req.body.gif = req.files.gif[0].filename : req.body.gif = req.body.gif_name
    req.files.video ? req.body.video = req.files.video[0].filename : req.body.video = req.body.content_name
    delete req.body.image_name
    delete req.body.gif_name
    delete req.body.content_name
    db('tb_movies')
        .where('url', req.body.url)
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update content success.",
                // count: data.length,
                data: data,
            });
        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update content failed.",
            // count: data.length,
            data: err,
        });
    });
});

// UPDATE IS PUBLIC/PRIVATE
router.patch('/update-is-public', (req, res) =>  {
    db('tb_movies')
        .where('id', req.body.id)
        .update('status', req.body.is_public_private)
        .then(data => {
            res.json({
                success: true,
                message: "Update content status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update content status failed",
                // count: data.length,
                data: err,
            });
        });
});

// UPDATE IS ACTIVE
router.post('/update-is-active', (req, res) =>  {
    db('banners')
        .where('id', req.body.id)
        .update('is_deleted', req.body.is_deleted)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banner status failed",
                // count: data.length,
                data: err,
            });
        });

});

// DELETE
router.delete('/delete/:id', (req, res) => {
    db('tb_movies').where({ id: req.params.id })
        .del()
        .then((data) => {
        res.json({
            success: true,
            message: "Delete content success",
            data: data,
        });
    });
});

module.exports = router;