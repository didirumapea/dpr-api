const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const slugify = require('slugify')
const cryptoRandomString = require('crypto-random-string');


router.get('/', (req, res) => {
    res.send('We Are In DASHBOARD Route')
})

// GET
router.get('/list', async (req, res) => {

    const views_content = await db('tb_movies')
        .sum('views as total');

    const total_content = await db('tb_movies')
        .count('* as total');

    const total_stream = await db('tb_stream')
        .count('* as total');

    const most_view_content = await db.select(
        'name',
        'views',
        'image',
        'type',
    )
        .from('tb_movies')
        .orderBy('views', 'desc')

    const result = {
        views_content: views_content,
        total_content: total_content,
        total_stream: total_stream,
        most_view_content: most_view_content,
    }

    res.json({
        success: true,
        message: "Success get data dashboard",
        data: result,
    });
    //
    // db.select(
    //     '*',
    // )
    //     .from('tb_category')
    //     .then(data => {
    //         res.json({
    //             success: data.length !== 0,
    //             message: data.length === 0 ? 'Dashboard still empty' : "Success get data dashboard",
    //             data: data,
    //         });
    //     })
    //     .catch((err) => {
    //         console.log(err)
    //         res.json({
    //             success: false,
    //             message: "ERROR",
    //             err_mes: err
    //         });
    //     })
});

module.exports = router;