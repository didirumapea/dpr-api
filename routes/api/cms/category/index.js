const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const slugify = require('slugify')
const cryptoRandomString = require('crypto-random-string');


router.get('/', (req, res) => {
    res.send('We Are In CATEGORY Route')
})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*',
    )
        .from('tb_category')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET BY URL
router.get('/list/url=:url/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_category')
        .where({ 'url': req.params.url })
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Category still empty' : "Success get data category",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data.length !== 0 ? paginator.data[0] : [],
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .whereRaw(`CONCAT_WS('', ref_no, regarding, letter_type, agenda_no, attachment, receive_from, hit_on_last_number, hit_on_the_next_number, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get document' : 'There are no document yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});

// ADD
router.post('/add', (req, res) => {
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    req.body.url = cryptoRandomString({length: 10, type: 'url-safe'});
    db('tb_category')
        .insert(req.body)
        .then(data1 => {
            res.json({
                success: true,
                message: "Add category success",
                data: data1
                // token: token
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Add category failed.",
                data: err,
            });
            console.log('category : '+err)
        });
});

// UPDATE
router.patch('/update', (req, res) =>  {
    // console.log(req.file)
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    db('tb_category')
        .where('url', req.body.url)
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update category success.",
                // count: data.length,
                data: data,
            });
        })
        .catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update category failed.",
            // count: data.length,
            data: err,
        });
    });
});

// UPDATE IS PUBLISH
router.post('/update-is-publish', (req, res) =>  {
    // console.log(req.body)
    db('banners')
        .where('id', req.body.id)
        .update('is_publish', req.body.is_publish)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status succedd.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banners status failed",
                // count: data.length,
                data: err,
            });
        });
});

// UPDATE IS ACTIVE
router.post('/update-is-active', (req, res) =>  {
    // console.log(req.body)
    db('banners')
        .where('id', req.body.id)
        .update('is_deleted', req.body.is_deleted)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banner status failed",
                // count: data.length,
                data: err,
            });
        });

});

// DELETE
router.delete('/delete/:id', (req, res) => {

    db('tb_category').where({ id: req.params.id })
        .del()
        .then((data) => {
        res.json({
            success: true,
            message: "Delete category success",
            data: data,
        });
    });
});

module.exports = router;