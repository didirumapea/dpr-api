const express = require('express');
const router = express.Router();
const moment = require('moment/moment');
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In CATEGORY Route')
})

// GET
router.get('/listing/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    const q = db.select(
        '*',
    )
        .from('tb_category');
        if (req.params.auth === 'false'){
            q.where('status', 'public')
        }
        q.orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Category still empty' : "Success get data category",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET VIDEO CATEGORY
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort/auth=:auth', async (req, res) => {
    let whereVar = {
        'type': 'video',
        'status': req.params.auth === 'false' ? 'public' : 'all',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);
    const category = await db.select(
        'id',
        'name',
        'url'
    )
        .from('tb_category')
    let ctgy = []
    for (const el of category) {
        const list = await db.select(
            '*',
            )
            .from('tb_movies')
            .where('category_id', el.id)
            .andWhere(whereVar)
            .orderBy('id', 'desc')

        // console.log(list)
        if (list.length !== 0){
            ctgy.push(
                {
                    category: {
                        name: el.name,
                        url: el.url
                    },
                    data: list
                }
            )
        }
    }
    res.json({
        success: true,
        message: "Success get data category",
        data: ctgy,
    });
});
// GET CATEGORY BY CATEGORY URL
router.get('/list/category_url=:category_url/type=:type/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    let whereVar = {
        't2.url': req.params.category_url,
        't1.type': req.params.type,
        't1.status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);

    db.select(
        't1.*',
        't2.url as category_url',
        't2.description as category_desc',
        't2.name as category_name',
    )
        .from('tb_movies as t1')
        .innerJoin('tb_category as t2', 't1.category_id', 't2.id')
        .where(whereVar)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Data still empty' : "Success get data",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .whereRaw(`CONCAT_WS('', ref_no, regarding, letter_type, agenda_no, attachment, receive_from, hit_on_last_number, hit_on_the_next_number, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get document' : 'There are no document yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});


module.exports = router;

