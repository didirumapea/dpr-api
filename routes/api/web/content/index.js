const express = require('express');
const router = express.Router();
const moment = require('moment/moment');
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In CONTENT Route')
})

// UPDATE VIEWS
router.patch('/update-views', (req, res) =>  {
    // console.log(req.body)
    db.select(
        'name',
        'views'
    )
        .from('tb_movies')
        .where('url', req.body.url)
        .limit(1)
        .then(result => {
            if (result.length > 0) {
                db('tb_movies')
                    .where('url', req.body.url)
                    .update('views', result[0].views += 1)
                    .then(data => {
                        res.json({
                            success: true,
                            message: "Update views success.",
                            count: data.length,
                            data: data,
                            result: result,
                        });
                    })
            }else{
                res.json({
                    success: true,
                    message: "data not found, update views failed.",
                });
            }

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update views failed",
                // count: data.length,
                data: err,
            });
        });
});
// GET VIDEO
router.get('/video/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    const whereVar = {
        'status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);
    db.select(
        '*',
    )
        .from('tb_movies')
        .where(whereVar)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET VIDEO BY MORE LIKE
router.get('/list/category_id=:category_id/type=:type/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    let whereVar = {
        'category_id': req.params.category_id,
        'type': req.params.type,
        'status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);
    db.select(
        '*'
    )
        .from('tb_movies')
        .where(whereVar)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Video still empty' : "Success get data video",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET VIDEO BY URL ID
router.get('/video/list/url=:url/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_movies')
        .where({ 'url': req.params.url })
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Video still empty' : "Success get data video",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .whereRaw(`CONCAT_WS('', ref_no, regarding, letter_type, agenda_no, attachment, receive_from, hit_on_last_number, hit_on_the_next_number, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get document' : 'There are no document yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});

module.exports = router;

