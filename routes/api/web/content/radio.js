const express = require('express');
const router = express.Router();
const moment = require('moment/moment');
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const setupPaginator = require('knex-paginator');
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In CONTENT RADIO Route')
})

// GET RADIO
router.get('/listing/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    let whereVar = {
        'type': 'radio',
        'status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);
    db.select(
        '*',
    )
        .from('tb_movies')
        .where(whereVar)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Radio still empty' : "Success get data radio",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET RADIO CATEGORY
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', async (req, res) => {
    let whereVar = {
        'type': 'radio',
        'status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);

    const category = await db.select(
        'id',
        'name',
        'url'
    )
        .from('tb_category')
    let ctgy = []
    for (const el of category) {
        whereVar.category_id = el.id
        const list = await db.select(
            '*',
        )
            .from('tb_movies')
            .where(whereVar)
        // console.log(list)
        if (list.length !== 0){
            ctgy.push(
                {
                    category: {
                        name: el.name,
                        url: el.url
                    },
                    data: list
                }
            )
        }
    }
    res.json({
        success: true,
        message: "Success get data category",
        data: ctgy,
    });
});
// GET RADIO BY URL ID
router.get('/list/url=:url/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_movies')
        .where({ 'url': req.params.url })
        .andWhere({ 'type': 'radio' })
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Video still empty' : "Success get data video",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET RADIO CATEGORY
router.get('/list/category/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', async (req, res) => {

    const category = await db.select(
        'id',
        'name',
        'url'
    )
        .from('tb_category')
    let ctgy = []
    for (const el of category) {
        const list = await db.select(
            '*',
        )
            .from('tb_movies')
            .where('category_id', el.id)
            .andWhere('type', 'radio')
        // console.log(list)
        if (list.length !== 0){
            ctgy.push(
                {
                    category: {
                        name: el.name,
                        url: el.url
                    },
                    data: list
                }
            )
        }
    }
    res.json({
        success: true,
        message: "Success get data radio category",
        data: ctgy,
    });
});


module.exports = router;

