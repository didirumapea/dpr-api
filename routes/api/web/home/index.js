const express = require('express');
const router = express.Router();
const moment = require('moment/moment');
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const setupPaginator = require('knex-paginator');
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In HOME Route')
})

router.post('/add-subscriber', (req, res) => {
    // console.log(req.body)
    db.select(
        'id',
        'email',
    )
        .from('subscribers')
        .where({
            'email': req.body.email,
        })
        .then(data => {
            if (data.length === 0){
                db('subscribers')
                    .insert(req.body)
                    .then(data => {
                        res.json({
                            success: true,
                            message: 'Add subscriber success',
                            count: data,
                            // data: result,
                        });
                    })
            }else{
                res.json({
                    success: false,
                    message: "Email already exists.",
                });
            }
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Add subsciber error",
                // count: data.length,
                data: err,
            });
        });
});

// GET
router.get('/list/type=:type/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    let whereVar = {
        'type': req.params.type,
        'status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);
    db.select(
        '*',
    )
        .from('tb_movies')
        .where(whereVar)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET VIDEO BY ID
router.get('/list/id=:id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('tb_document')
        .where({'id': req.params.id})
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Document still empty' : "Success get data document",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET NEWEST VIDEO
router.get('/list/newest/video/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    let whereVar = {
        'type': 'video',
        'status': req.user ? 'all' : 'public',
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === 'all') && delete whereVar[key]);
    db.select(
        '*',
    )
        .from('tb_movies')
        .where(whereVar)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET TRENDING VIDEO
router.get('/list/trending/video/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*',
    )
        .from('tb_movies')
        .where('type', 'video')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});
// GET VIDEO HOME LIMIT 3
router.get('/list/limit=:limit/column-sort=:col_sort/sort=:sort', async (req, res) => {

    const category = await db.select(
        'id',
        'name',
        'url'
    )
        .from('tb_category')
        .limit(req.params.limit)
    let ctgy = []
    for (const el of category) {
        const list = await db.select(
            '*',
        )
            .from('tb_movies')
            .where('category_id', el.id)
        // console.log(list)
        if (list.length !== 0){
            ctgy.push(
                {
                    category: {
                        name: el.name,
                        url: el.url
                    },
                    data: list
                }
            )
        }
    }
    res.json({
        success: true,
        message: "Success get data category",
        data: ctgy,
    });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', async (req, res) => {
    const radio = await db.select(
        '*'
    )
        .from('tb_movies')
        .whereRaw(`CONCAT_WS('', name, description) LIKE ?`, [`%${req.params.value}%`])
        .andWhere('type', 'radio')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            return {
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get content' : 'There are no content yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            };
        });
    const video = await db.select(
        '*'
    )
        .from('tb_movies')
        .whereRaw(`CONCAT_WS('', name, description) LIKE ?`, [`%${req.params.value}%`])
        .andWhere('type', 'video')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            return {
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get content' : 'There are no content yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            };
        });
    const stream = await db.select(
        '*'
    )
        .from('tb_stream')
        .whereRaw(`CONCAT_WS('', name, description) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            return {
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get stream' : 'There are no stream yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            };
        });
    res.json({
        radio: radio,
        video: video,
        stream: stream,
    })
    // console.log(q)
});
// GET VIDEO STREAM BY POSITION
router.get('/list/stream/position=:position/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    const listWhere = {
        'type': 'video',
        'position': req.params.position
    }
    db.select(
        '*',
    )
        .from('tb_stream')
        .where(listWhere)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length === 0 ? 'Movie still empty' : "Success get data movies",
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "ERROR",
                err_mes: err
            });
        })
});


module.exports = router;

