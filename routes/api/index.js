const express = require('express');
const router = express.Router();
const checkAuthAdmin = require('../../middleware/check-auth-admin')
const url1 = '/apps';
const url2 = '/cms';
const url3 = '/web';


// region CMS
// ------------------------------------------------ CMS -----
// PATH FOLDER
// BASE
const cms = require('./cms');
// DASHBOARD
const cms_dashboard = require('./cms/dashboard');
// CONTENT
const cms_content = require('./cms/content');
// STREAM
const cms_stream = require('./cms/stream');
// CATEGORY
const cms_category = require('./cms/category');
// AUTH
const cms_auth = require('./cms/auth');
// USERS
const cms_users = require('./cms/users');
// USERS ADMIN
const cms_users_admin = require('./cms/users/admin');


// PATH URL/API
// BASE
router.use(`${url2}/`, cms);
// DASHBOARD
router.use(`${url2}/dashboard`, cms_dashboard);
// CONTENT
router.use(`${url2}/content`, cms_content);
// STREAM
router.use(`${url2}/stream`, cms_stream);
// CATEGORY
router.use(`${url2}/category`, cms_category);
// AUTH
router.use(`${url2}/auth`, cms_auth);
// USERS
router.use(`${url2}/users`, cms_users);
// USERS ADMIN
router.use(`${url2}/users/admin`, cms_users_admin);
//endregion


// region APPS
// --- -------------------------------------------- APPS -----------------------------------------------
// PATH FOLDER
// PAYROLL
const apps_payroll = require('./apps/payroll');
// AUTH
const apps_auth = require('./apps/auth');
const apps_auth_employees = require('./apps/auth/employees');
// EMPLOYEES
const apps_employees = require('./apps/employees');

// PATH URL / API
// PAYROLL
router.use(`${url1}/payroll`, apps_payroll);
//region AUTH
router.use(`${url1}/auth`, apps_auth);
router.use(`${url1}/auth/employees`, apps_auth_employees);
// EMPLOYEES
router.use(`${url1}/employees`, apps_employees);

// endregion

// region WEB
// --- -------------------------------------------- WEB -----------------------------------------------
// PATH FOLDER
// HOME
const web_home = require('./web/home');
// CONTENT
const web_content = require('./web/content');
// CONTENT
const web_content_radio = require('./web/content/radio');
// CATEGORY
const web_category = require('./web/category');
// LIVESTREAM
const web_livestream = require('./web/livestream');
// LIVESTREAM RADIO
const web_livestream_radio = require('./web/livestream/radio');

// PATH URL / API
// HOME
router.use(`${url3}/home`, checkAuthAdmin, web_home);
// CONTENT
router.use(`${url3}/content`, checkAuthAdmin, web_content);
// CONTENT RADIO
router.use(`${url3}/content/radio`, checkAuthAdmin, web_content_radio);
// CATEGORY
router.use(`${url3}/category`, checkAuthAdmin, web_category);
// LIVESTREAM VIDEO
router.use(`${url3}/livestream`, checkAuthAdmin, web_livestream);
// LIVESTREAM RADIO
router.use(`${url3}/livestream/radio`, checkAuthAdmin, web_livestream_radio);

// endregion

module.exports = router;