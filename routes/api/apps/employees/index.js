const express = require('express');
const router = express.Router();
const moment = require('moment/moment');
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const setupPaginator = require('knex-paginator');
setupPaginator(db);

router.post('/attendance/check-in', (req, res) => {
    let attendanceData = {
        employees_id: req.userData.employees_id,
        attendance_date: date.utc7dateOnly(),
        check_in: date.utc(),
    }
    db.select(
        'id',
        'check_in',
        'status'
    )
        .from('attendance')
        .where({
            'employees_id': req.userData.employees_id,
        })
        .andWhere('attendance_date', date.utc7dateOnly())
        .then(data => {
            if (data.length === 0){
                db('attendance')
                    .insert(Object.assign(attendanceData, req.body))
                    .then(data => {
                        res.json({
                            success: true,
                            message: 'Check in berhasil',
                            count: data,
                            // data: result,
                        });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({
                            success: false,
                            message: "Check in gagal.",
                            // count: data.length,
                            data: err,
                        });
                    });
            }else{
                if (data[0].check_in === '00:00:00'){
                    db('attendance')
                        .where({
                            'id': data[0].id,
                            'employees_id': req.userData.employees_id,
                        })
                        .update({
                            check_in : date.utc()
                        })
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Update check in berhasil.",
                                data: data2,
                            });
                        })
                }else{

                    res.json({
                        success: false,
                        message: data[0].status === 'hadir' ? 'Kamu sudah check in' : `Kamu sedang ${data[0].status} hari ini.`,
                        // data: data,
                    });
                }
            }
        })
});

router.post('/attendance/check-out', (req, res) => {
    // console.log(req.userData.id)

    db.select(
        'id',
        'check_in',
        'check_out',
    )
        .from('attendance')
        .where({
            'employees_id': req.userData.employees_id,
            'id': req.body.attendance_id,
        })
        .then(data => {
            // console.log(data[0].check_out)
            if (data.length !== 0){
                if ((data[0].check_in !== 'Invalid date' && (data[0].check_out === 'Invalid date' || data[0].check_out === null || data[0].check_out === '00:00:00')) || data[0].check_in === '00:00:00'){
                    db('attendance')
                        .where({
                            'id': data[0].id,
                            'employees_id': req.userData.employees_id,
                        })
                        .update({
                            check_out : date.utc()
                        })
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Update check out time success.",
                                data: data2,
                            });
                        })
                        .catch((err) => {
                            console.log(err)
                            res.json({
                                success: false,
                                message: "Check out failed.",
                                // count: data.length,
                                data: err,
                            });
                        });
                }else{
                    res.json({
                        success: false,
                        message: "You already checked out",
                        // data: data2,
                    });
                }
            }else{
                res.json({
                    success: false,
                    message: "You haven't checked in yet",
                    // data: data2,
                });
            }
        })
});

router.post('/attendance/overtime-in', (req, res) => {
    // let attendanceData = {
    //     employees_id: req.userData.employees_id,
    //     overtime_in: date.utc(),
    // }
    db.select(
        'id',
        'check_out',
        'overtime_in',
    )
        .from('attendance')
        .where({
            'id': req.body.attendance_id,
            'employees_id': req.userData.employees_id,
        })
        .then(data => {
            if (data.length !== 0){
                if (data[0].check_out === 'Invalid date'){
                    res.json({
                        success: false,
                        message: 'Kamu belum check out',
                    });
                }else{
                    if (data[0].overtime_in === 'Invalid date'){
                        db('attendance')
                            .where({
                                'id': data[0].id,
                                'employees_id': req.userData.employees_id,
                            })
                            .update({
                                overtime_in : date.utc()
                            })
                            .then(data2 => {
                                res.json({
                                    success: true,
                                    message: "Update overtime in time success.",
                                    data: data2,
                                });
                            })
                    }else{
                        res.json({
                            success: false,
                            message: "Kamu sudah melakukan overtime in",
                            // data: data2,
                        });
                    }
                }

            }else{
                res.json({
                    success: false,
                    message: "Kamu Belum Check in dan Check out hari ini",
                    // data: data2,
                });
            }
        })
});

router.post('/attendance/overtime-out', (req, res) => {
    // console.log(req.userData.id)

    db.select(
        'id',
        'overtime_in',
        'overtime_out',
        'overtime_approved_by',
    )
        .from('attendance')
        .where({
            'id': req.body.attendance_id,
            'employees_id': req.userData.employees_id,
        })
        .then(data => {
            if (data.length !== 0){
                if ((
                    data[0].overtime_approved_by !== null &&
                    data[0].overtime_in !== 'Invalid date' && (
                    data[0].overtime_out === 'Invalid date' ||
                    data[0].overtime_out === null ||
                    data[0].overtime_out === '00:00:00')) ||
                    data[0].overtime_in === '00:00:00'){
                    db('attendance')
                        .where({
                            'id': data[0].id,
                            'employees_id': req.userData.employees_id,
                        })
                        .update({
                            overtime_out : date.utc()
                        })
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Update overtime out berhasil.",
                                data: data2,
                            });
                        })
                        .catch((err) => {
                            console.log(err)
                            res.json({
                                success: false,
                                message: "Overtime out gagal.",
                                // count: data.length,
                                data: err,
                            });
                        });
                }else{
                    res.json({
                        success: false,
                        message: data[0].overtime_approved_by === null ? `Your overtime haven't approved.` : "Kamu sudah overtime out",
                        // data: data2,
                    });
                }
            }else{
                res.json({
                    success: false,
                    message: "Kamu belum melakukan overtime in",
                    // data: data2,
                });
            }
        })
});

router.post('/attendance/overtime-permission', (req, res) => {

    db.select(
        't1.id',
        'overtime_approved_by',
        't2.name as employees_name',
        // 'check_out',
    )
        .from('attendance as t1')
        .leftJoin('employees as t2', 't1.overtime_approved_by', 't2.id')
        .where({
            't1.id': req.body.attendance_id,
            'employees_id': req.body.employees_id,
        })
        .then(data => {
            if (data.length !== 0){
                if (data[0].overtime_approved_by === null){
                    db('attendance')
                        .where({
                            'id': req.body.attendance_id,
                            'employees_id': req.body.employees_id,
                        })
                        .update({
                            overtime_approved_by : req.userData.employees_id
                        })
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Update overtime permission success.",
                                data: data2,
                            });
                        })
                        .catch((err) => {
                            console.log(err)
                            res.json({
                                success: false,
                                message: "Check out failed.",
                                // count: data.length,
                                data: err,
                            });
                        });
                }else{
                    res.json({
                        success: false,
                        message: `Overtime has been Approved By ${data[0].overtime_approved_by === req.userData.employees_id ? 'Me' : data[0].employees_name}`,
                        // data: data2,
                    });
                }
            }else{
                res.json({
                    success: false,
                    message: "Employees haven't checked in today",
                    // data: data2,
                });
            }
        })
});

router.get('/attendance/details', (req, res) => {
    db.select(
        'id',
        'check_in',
        'check_out',
        'status',
        'attendance_type',
    )
        .from('attendance')
        .where({
            'employees_id': req.userData.employees_id,
            'attendance_date': date.utc7dateOnly(),
        })
        .then(data => {
            if (data.length !== 0){
                // data[0].check_in = data[0].check_in !== 'Invalid date' ? moment(data[0].check_in).format() : data[0].check_in
                // data[0].check_out = data[0].check_out !== 'Invalid date' ? moment(data[0].check_out).format() : data[0].check_out
                res.json({
                    success: true,
                    message: "Get details time work success.",
                    data: data,
                });
            }else{
                res.json({
                    success: false,
                    message: "Get details time work not available today, please check in first",
                    data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get details time work failed",
                data: err,
            });
        })
});

router.post('/leaves/add', (req, res) => {
    req.body.employees_id = req.userData.employees_id
    req.body.status = 'pending'

    db('leaves')
        .insert(req.body)
        .then(data => {
            res.json({
                success: true,
                message: 'Daftar cuti  berhasil',
                count: data,
                // data: result,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Daftar cuti gagal.",
                // count: data.length,
                data: err,
            });
        });
})

router.post('/attendance/permission', (req, res) => {
    req.body.employees_id = req.userData.employees_id

    db('attendance')
        .insert(req.body)
        .then(data => {
            res.json({
                success: true,
                message: 'Daftar izin berhasil',
                count: data,
                // data: result,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Daftar izin gagal.",
                // count: data.length,
                data: err,
            });
        });
})

router.get('/history/status=:status/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    if (req.params.status !== 'cuti'){
        db.select(
            't2.name as employees_name',
            't1.id',
            't1.attendance_type',
            't1.attendance_date',
            `t1.check_in`,
            't1.check_out',
            't1.overtime_in',
            't1.overtime_out',
            't1.status',
            // 'check_out',
        )
            .from('attendance as t1')
            .leftJoin('employees as t2', 't1.employees_id', 't2.id')
            .where({
                't1.employees_id': req.userData.employees_id,
                't1.status': req.params.status,
            })
            .orderBy('t1.'+req.params.col_sort, req.params.sort)
            .paginate(req.params.limit, req.params.page, true)
            .then(paginator => {
                res.json({
                    success: true,
                    message: paginator.data.length === 0 ? 'History still empty' :"Success get data history",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            })
    }else{
        db.select(
            't2.name as employees_name',
            't1.id',
            't1.leave_type',
            't1.from',
            `t1.to`,
            't1.reason',
            't1.status',
            't3.name as approved_by',
        )
            .from('leaves as t1')
            .leftJoin('employees as t2', 't1.employees_id', 't2.id')
            .leftJoin('admin as t3', 't1.approved_by', 't3.id')
            .where({
                't1.employees_id': req.userData.employees_id,
            })
            .orderBy('t1.'+req.params.col_sort, req.params.sort)
            .paginate(req.params.limit, req.params.page, true)
            .then(paginator => {
                res.json({
                    success: true,
                    message: paginator.data.length === 0 ? 'Leave history still empty' :"Success get data leave history",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            })
            .catch(err => {
                console.log(err)
                res.json({
                    success: false,
                    message: err,
                });
            })
    }
});

router.get('/leaves/status/id=:id', (req, res) => {
    db.select(
        't2.name as employees_name',
        't1.id',
        't1.leave_type',
        't1.from',
        `t1.to`,
        't1.reason',
        't1.status',
        't3.name as approved_by',
        't2.amount_of_leave',
        't2.remaining_leave',
        't2.joining_date',
    )
        .from('leaves as t1')
        .leftJoin('employees as t2', 't1.employees_id', 't2.id')
        .leftJoin('admin as t3', 't1.approved_by', 't3.id')
        .where({
            't1.employees_id': req.userData.employees_id,
            't1.id': req.params.id,
        })
        .then(data => {
            // let a = moment(data[0].to);
            // let b = moment(data[0].from);
            // // a.diff(b, 'days')   // =1
            // console.log(a.diff(b, 'days')+1)
            data[0].expired_leave_date = moment(data[0].joining_date).add(1, 'years').format('YYYY-MM-DD HH:mm:ss')
            res.json({
                success: true,
                message: data.length !== 0 ? `Get leave status succees` : 'Leave status not found, please create first',
                data: data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: 'Get leave status failed',
                data: err,
            });
        })
});

// REPORT EVERY MONTH
router.get('/report/month/status=:status', (req, res) => {
    if (req.params.status !== 'cuti'){
        db.raw(
            `SELECT DATE_FORMAT(attendance_date, "%m-%Y") AS Month, count(*) as ${req.params.status}
        FROM attendance
        where employees_id = ${req.userData.employees_id} and status = '${req.params.status}'
        GROUP BY DATE_FORMAT(attendance_date, "%m-%Y");`
        )
            .then(data => {
                res.json({
                    success: true,
                    message: data.length !== 0 ? `Get report month succees` : 'report month not found, please create first',
                    data: data[0],
                });
            })
            .catch((err) => {
                res.json({
                    success: false,
                    message: 'Get report month status failed',
                    data: err,
                });
            })
    }else{
        db.raw(
            `SELECT DATE_FORMAT(created_at, "%m-%Y") AS Month, count(*) as ${req.params.status}
        FROM leaves
        where employees_id = ${req.userData.employees_id}
        GROUP BY DATE_FORMAT(created_at, "%m-%Y");`
        )
            .then(data => {
                res.json({
                    success: true,
                    message: data.length !== 0 ? `Get report month succees` : 'report month not found, please create first',
                    data: data[0],
                });
            })
            .catch((err) => {
                res.json({
                    success: false,
                    message: 'Get report month status failed',
                    data: err,
                });
            })
    }
});

// REPORT BY DATE #2020-08
router.get('/report/month/date=:date', (req, res) => {

    db.raw(
        `SELECT  (
            SELECT COUNT(*)
            FROM   attendance
                where status = 'izin' 
                and employees_id = ${req.userData.employees_id} 
                and attendance_date like '%${req.params.date}%'
        ) AS izin,
        (
            SELECT COUNT(*)
            FROM   attendance
                where status = 'hadir'
                and employees_id = ${req.userData.employees_id}  
                and attendance_date like '%${req.params.date}%'
        ) AS hadir,
        (
            SELECT COUNT(*)
            FROM   leaves
                where employees_id = ${req.userData.employees_id}
                and created_at like '%${req.params.date}%'
                and status = 'approved'
        ) AS cuti,
        (
        SELECT COUNT(*)
        FROM attendance
        where employees_id = ${req.userData.employees_id}
        and attendance_date like '%${req.params.date}%'
        and overtime_out IS NOT NULL
    ) AS lembur;`
    )
        .then(data => {
            data[0][0].attendance_date = req.params.date
            res.json({
                success: true,
                message: data.length !== 0 ? `Get report month succees` : 'report month not found, please create first',
                data: data[0],
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: 'Get report month status failed',
                data: err,
            });
        })
});

module.exports = router;

