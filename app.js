const express = require('express');
const app = express();
const path = require('path');
require("@babel/register")({
    presets: ["@babel/preset-env"]
});
require("regenerator-runtime/runtime");
const apiRoute = require('./routes/api');
const version = 'v1/'
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();

const checkHeader = require('./middleware/check-header')
// const sshtunnel = require('./database/ssh-tunnel-db')

// use it before all route definitions
app.use(cors());
app.use(express.static('/mnt/cdn'));

// app.get('/render', function (req, res) {
//     res.sendFile(path.join(__dirname+'/public/pdfs/basics.pdf'));
// })

app.get('/', function (req, res) {
    res.send('Hello DPR API')
})

app.get('/dpr-files', function (req, res) {
    res.send('Hello CDN DPR!')
})

app.get('/api/'+version, checkHeader, function (req, res) {
    res.send('Hello DPR Api With Version')
})

// catch 404 and forward to error handler global
// app.use(function(req, res, next) {
//     return res.status(404).json({
//         success : 'false',
//         message :'Request tidak di Temukan'
//     });
//     let err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });




app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb',
    parameterLimit: 1000000
}));
app.use(bodyParser.json({
    limit: '50mb'
}));


// app.use('/api/'+version, checkHeader, apiRoute); //ex localhost:3002/api/merchant/get/list/1
app.use('/api/'+version, apiRoute); //ex localhost:3002/api/merchant/get/list/1


app.use(cookieParser());
// app.listen('3002')
const PORT = process.env.PORT || process.env.PORT
app.listen(PORT, console.log(`Server started on port ${PORT}`))