
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.string('position')
            .after('banner')
            .defaultTo(null);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.dropColumn('position');
    })
};
