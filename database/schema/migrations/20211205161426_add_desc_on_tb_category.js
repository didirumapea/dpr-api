
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_category', (table) => {
        table.string('description')
            .after('name')
            .defaultTo(null);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_category', (table) => {
        table.dropColumn('description');
    })
};
