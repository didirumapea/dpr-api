
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.enum('status', ['private', 'public'])
            .defaultTo('private')
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.text('description')
            .defaultTo(null)
            .alter();
    })
};
