
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.text('description')
            .defaultTo(null)
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.string('description')
            .defaultTo(null)
            .alter();
    })
};
