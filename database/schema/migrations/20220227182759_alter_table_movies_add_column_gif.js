
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_movies', (table) => {
        table.string('gif')
            .defaultTo(null)
            .after('image')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_movies', (table) => {
        table.dropColumn('gif');
    })
};
