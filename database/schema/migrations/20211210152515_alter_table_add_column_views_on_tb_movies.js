
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_movies', (table) => {
        table.integer('views')
            .after('video')
            .defaultTo(0);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_movies', (table) => {
        table.dropColumn('views');
    })
};
