exports.up = function(knex, Promise) {
    return knex.schema.createTable('tb_admin', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.string('email')
        table.string('password')
        table.string('name')
        table.string('phone')
        table.string('address')
        table.string('images')
        table.enum('role', ['admin', 'super_admin'])
            .defaultTo('admin')
        table.specificType('is_deleted', 'tinyint(1)')
            .notNullable()
            .defaultTo(0);
        table.dateTime('created_at')
            .notNullable()
            .defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at')
            .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
        .catch((err) => {
            console.log(err)
        })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('tb_admin')
};
