
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_movies', (table) => {
        table.enum('status', ['private', 'public'])
            .defaultTo('private')
            .after('views')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_movies', (table) => {
        table.dropColumn('status');
    })
};
