exports.up = function(knex, Promise) {
    return knex.schema.createTable('tb_stream', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.integer('category_id')
            .defaultTo(0)
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('tb_category')
            .onDelete('CASCADE')
            .index();
        table.string('name')
        table.string('description')
        table.string('slug')
        table.string('url')
        table.string('url_stream')
        table.string('image')
        table.enum('type', ['video', 'radio'])
            .notNullable()
        table.string('quality')
        table.string('release_year')
        table.string('language')
        table.string('banner')
        table.specificType('status', 'tinyint(1)')
            .notNullable()
            .defaultTo(0)
        table.specificType('is_deleted', 'tinyint(1)')
            .notNullable()
            .defaultTo(0);
        table.dateTime('created_at')
            .notNullable()
            .defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at')
            .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
        .catch((err) => {
            console.log(err)
        })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('tb_stream')
};
