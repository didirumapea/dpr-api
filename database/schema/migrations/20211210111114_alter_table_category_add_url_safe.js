
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_category', (table) => {
        table.string('url')
            .after('slug')
            .defaultTo(null);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_category', (table) => {
        table.dropColumn('url');
    })
};
