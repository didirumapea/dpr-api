
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_category', (table) => {
        table.text('description')
            .defaultTo(null)
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_category', (table) => {
        table.string('description')
            .defaultTo(null)
            .alter();
    })
};
