
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.integer('views')
            .defaultTo(0)
            .after('release_year')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('tb_stream', (table) => {
        table.dropColumn('views');
    })
};
