const jwt = require('jsonwebtoken')
const config = require('../config')

module.exports = (req, res, next) => {
    try{
        // console.log(req.header.authorization)
        const token = req.headers.authorization.split(' ')[1]
        // const token = req.headers.authorization
        req.user = jwt.verify(token, config.jwtSecretKeyAdmin);
        next();
    }catch (error) {
        let err
        if (error.message === 'jwt expired'){
            err = 'Token Expired'
            return res.status(401).json({
                // message: 'auth Failed'
                message: err
            })
        }else{
            // err = 'Auth Failed'
            next();
        }

        // return res.status(401).json({
        //     message: 'auth Failed'
        // })
    }
}